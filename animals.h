///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Melvin Alhambra <@melvin42@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo 06-02-2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
enum Gender {MALE, FEMALE};
enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};
enum CatBreed {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

char* genderName (enum Gender gender);
char* colorName (enum Color color);
char* catBreedName (enum CatBreed catbreed);
char* fixedName(bool isFixed);
