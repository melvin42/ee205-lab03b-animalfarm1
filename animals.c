///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @Melvin Alhambra <@melvin42@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @06/02/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdbool.h>
#include "animals.h"
#include "cat.h"


char* genderName (enum Gender gender){
   char* Gender;
   switch(gender){
      case MALE:
         return "Male";
         break;
      case FEMALE:
         return "Female";
         break;
   }
   return Gender;
   return NULL;
}

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
   char* Color;
   switch(color){
      case BLACK:
         return "Black";
         break;
      case WHITE:
         return "White";
         break;
      case RED:
         return "Red";
         break;
      case BLUE:
         return "Blue";
         break;
      case GREEN:
         return "Green";
         break;
      case PINK:
         return "Pink";
         break;
   }
   return Color;
   return NULL;
}

char* catBreedName (enum CatBreed catbreed){
   char* CatBreed;
   switch(catbreed){
      case MAIN_COON:
         return "Main Coon";
         break;
      case MANX:
         return "Manx";
         break;
      case SHORTHAIR:
         return "Shorthair";
         break;
      case PERSIAN:
         return "Persian";
         break;
      case SPHYNX:
         return "Sphynx";
         break;
   }
   return CatBreed;
   return NULL; // We should never get here
}

char* fixedName (bool isFixed){
   switch(isFixed){
      case true:
         return "Yes";
         break;
      case false:
         return "No";
         break;
   }
   return NULL;
}
